<?php 
class Anggota_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    public function get_anggota_id($id){
        $query = $this->db->get_where('anggota', array('id'=>$id));
        return $query->row();
    }

    public function create_anggota(){
        //  nama_lengkap    no_ktp  tempat_lahir    tanggal_lahir   alamat  nomor_telepon   email   agama   pekerjaan   jenis_kelamin   keterangan
        return $this->db->insert('anggota', [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'no_ktp' => $this->input->post('no_ktp'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'alamat' => $this->input->post('alamat'),
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'email' => $this->input->post('email'),
            'agama' => $this->input->post('agama'),
            'pekerjaan' => $this->input->post('pekerjaan'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'keterangan' => $this->input->post('keterangan')      
        ]);
    }

    public function delete_anggota($id){
        $this->db->where('id', $id)->delete('anggota');
        return true;
    }

    public function update_anggota(){
        $data = [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'no_ktp' => $this->input->post('no_ktp'), 
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'alamat' => $this->input->post('alamat'),
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'email' => $this->input->post('email'),
            'agama' => $this->input->post('agama'),
            'pekerjaan' => $this->input->post('pekerjaan'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'keterangan' => $this->input->post('keterangan')  
        ];
        return $this->db->where('id', $this->input->post('id'))->update('anggota', $data);
    }

    public function get_anggota(){
       return $this->db->order_by('id')->get('anggota')->result();
    }
}