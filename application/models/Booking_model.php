<?php 
class Booking_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    public function get_booking_id($id){
        $query = $this->db->get_where('booking_kegiatan', array('id'=>$id));
        return $query->row();
    }

    public function create_booking(){
        //booking_kegiatan
        //nama_lengkap  alamat_acara    nomor_telepon   tanggal_acara   jenis_kegiatan  waktu_pelaksanaan   keterangan
        return $this->db->insert('booking_kegiatan', [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'alamat_acara' => $this->input->post('alamat_acara'), 
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'tanggal_acara' => $this->input->post('tanggal_acara'),
            'jenis_kegiatan' => $this->input->post('jenis_kegiatan'),
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'keterangan' => $this->input->post('keterangan')
        ]);
    }

    public function delete_booking($id){
        $this->db->where('id', $id)->delete('booking_kegiatan');
        return true;
    }

    public function update_booking(){
        $data = [
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'alamat_acara' => $this->input->post('alamat_acara'), 
            'nomor_telepon' => $this->input->post('nomor_telepon'),
            'tanggal_acara' => $this->input->post('tanggal_acara'),
            'jenis_kegiatan' => $this->input->post('jenis_kegiatan'),
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'keterangan' => $this->input->post('keterangan')
        ];
        return $this->db->where('id', $this->input->post('id'))->update('booking_kegiatan', $data);
    }

    public function get_booking(){
       return $this->db->order_by('id')->get('booking_kegiatan')->result();
    }
}