<?php 
class Kegiatan_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }

    public function get_kegiatan_id($id){
        $query = $this->db->get_where('kegiatan', array('id'=>$id));
        return $query->row();
    }

    public function create_kegiatan(){
        //kegiatan
        //tanggal_kegiatan  keterangan  waktu_pelaksanaan   tempat_pelaksanaan
        return $this->db->insert('kegiatan', [
            'tanggal_kegiatan' => $this->input->post('tanggal_kegiatan'),
            'keterangan' => $this->input->post('keterangan'), 
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'tempat_pelaksanaan' => $this->input->post('tempat_pelaksanaan')
        ]);
    }

    public function delete_kegiatan($id){
        $this->db->where('id', $id)->delete('kegiatan');
        return true;
    }


    public function update_kegiatan(){
        $data = [
            'tanggal_kegiatan' => $this->input->post('tanggal_kegiatan'),
            'keterangan' => $this->input->post('keterangan'), 
            'waktu_pelaksanaan' => $this->input->post('waktu_pelaksanaan'),
            'tempat_pelaksanaan' => $this->input->post('tempat_pelaksanaan')  
        ];
        return $this->db->where('id', $this->input->post('id'))->update('kegiatan', $data);
    }

    public function get_kegiatan(){
       return $this->db->order_by('id')->get('kegiatan')->result();
    }
}