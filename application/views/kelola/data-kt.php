<div class="container">
    <div class="row justify-content-md-center">
        <center><h1>Data Booking Kegiatan</h1></center>
        
        <?php echo validation_errors(); ?>
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <a href="<?php echo base_url() ?>kelola"><button class="btn btn-md btn-success">Data Kegiatan Karang Taruna</button></a><br><br>
            <?php echo (isset($kegiatans)) ? form_open('kelola/update_kegiatan') : form_open('kelola/tambah_kegiatan_kt') ?>       
            <div class="form-group">
                <label>Acara</label>
                <input type="text" class="form-control" name="acara" placeholder="Acara" value="<?php echo (isset($kegiatans)) ? $kegiatans->acara : "" ;?>">
            </div>
            <div class="form-group">
                <label>Jenis Kegiatan</label>
                <input type="text" class="form-control" name="jenis_kegiatan" placeholder="Jenis Kegiatan" value="<?php echo (isset($kegiatans)) ? $kegiatans->jenis_kegiatan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi" value="<?php echo (isset($kegiatans)) ? $kegiatans->deskripsi : "" ;?>">
            </div>
            <div class="form-group">
                <label>Tempat Pelaksanaan</label>
                <input type="text" class="form-control" name="tempat_pelaksanaan" placeholder="Tempat Pelaksanaan" value="<?php echo (isset($kegiatans)) ? $kegiatans->tempat_pelaksanaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Waktu Pelaksanaan</label>
                <input type="text" class="form-control" name="waktu_pelaksanaan" placeholder="Waktu Pelaksanaan" value="<?php echo (isset($kegiatans)) ? $kegiatans->waktu_pelaksanaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Dokumentasi</label>
                <input type="text" class="form-control" name="dokumentasi" placeholder="Dokumentasi" value="<?php echo (isset($kegiatans)) ? $kegiatans->dokumentasi : "" ;?>">
            </div>
            <input type="hidden" class="form-control" name="id" value="<?php echo (isset($kegiatans)) ? $kegiatans->id : "" ;?>">
            <input type="submit" name="submit" value="Update/Tambah Data Kegiatan Karang Taruna" class="btn btn-primary btn-block">
        <?php echo form_close(); ?>
        </div>
        <div class="col-md-2">
        </div>
        
    </div>
</div>
