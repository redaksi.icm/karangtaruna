<div class="container">
<?php $this->load->view('templates/ms'); ?>

<center><h2>Data Kegiatan Karang Taruna</h2></center>
<br>
<a href="<?php echo base_url() ?>kelola/tambah_kegiatan_kt"><button class="btn btn-md btn-success">Tambah Data Karang Taruna</button></a><br><br>
    <table id="example" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Acara</th>
                    <th>Jenis Kegiatan</th>
                    <th>Deskripsi</th>
                    <th>Tempat Pelaksanaan</th>
                    <th>Waktu Pelaksanaan</th>
                    <th>Dokumentasi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($kegiatans as  $kegiatan) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $kegiatan->acara ?></td>
                    <td><?= $kegiatan->jenis_kegiatan ?></td>
                    <td><?= $kegiatan->deskripsi ?></td>
                    <td><?= $kegiatan->tempat_pelaksanaan ?></td>
                    <td><?= $kegiatan->waktu_pelaksanaan ?></td>
                    <td><?= $kegiatan->dokumentasi ?></td>
                    <td style="text-align: center"><a href="<?php echo base_url() ?>kelola/edit_kegiatan/<?= $kegiatan->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a><a href="<?php echo base_url() ?>kelola/hapus_kegiatan/<?= $kegiatan->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>


<center><h2>Data Pendaftar Kegiatan Karang Taruna</h2></center>
<br>
<a href="<?php echo base_url() ?>kelola/tambah_anggota_kt"><button class="btn btn-md btn-success">Tambah Data Anggota Karang Taruna</button></a><br><br>
    <table id="example1" class="table table-striped table-bordered" style="width: 100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>Nomor Telepon</th>
                    <th>Email</th>
                    <th>Jenis Kelamin</th>
                    <th>Pekerjaan</th>
                    <th>Kegiatan yang Diikuti</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; foreach ($pendaftars as  $pendaftar) :?>
                <tr>
                    <td><?= $no ?></td>
                    <td><?= $pendaftar->nama_lengkap ?></td>
                    <td><?= $pendaftar->alamat ?></td>
                    <td><?= $pendaftar->nomor_telepon ?></td>
                    <td><?= $pendaftar->email ?></td>
                    <td><?= $pendaftar->jenis_kelamin ?></td>
                    <td><?= $pendaftar->pekerjaan ?></td>
                    <td><?= $pendaftar->kegiatan ?></td>
                    <td style="text-align: center"><a href="<?php echo base_url() ?>kelola/edit_anggota/<?= $pendaftar->id ?>"><button class="btn btn-xs btn-warning" style="font-size: 9px">Edit</button></a><a href="<?php echo base_url() ?>kelola/hapus_anggota/<?= $pendaftar->id ?>"><button class="btn btn-xs btn-danger" style="font-size: 9px">Hapus</button></a></td>
                </tr>
                <?php $no++; endforeach; ?>
            </tbody>
    </table>
</div>
<br><br>
