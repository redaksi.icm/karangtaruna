<div class="container">
    <div class="row justify-content-md-center">
        <center><h1>Data Pendaftaran Kegiatan Karang Taruna</h1></center>
        
        <?php echo validation_errors(); ?>
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <a href="<?php echo base_url() ?>kelola"><button class="btn btn-md btn-success">Lihat Kelola</button></a><br><br>
            <?php echo (isset($pendaftar)) ? form_open('kelola/update_anggota') : form_open('kelola/tambah_anggota_kt') ?>              
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo (isset($pendaftar)) ? $pendaftar->nama_lengkap : "" ;?>">
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo (isset($pendaftar)) ? $pendaftar->alamat : "" ;?>">
            </div>
            <div class="form-group">
                <label>Nomor Telepon</label>
                <input type="text" class="form-control" name="nomor_telepon" placeholder="Nomor Telepon" value="<?php echo (isset($pendaftar)) ? $pendaftar->nomor_telepon : "" ;?>">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo (isset($pendaftar)) ? $pendaftar->email : "" ;?>">
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                  <option value="laki-laki">Laki-laki</option>
                  <option value="perempuan">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label>Pekerjaan</label>
                <input type="text" class="form-control" name="pekerjaan" placeholder="Pekerjaan" value="<?php echo (isset($pendaftar)) ? $pendaftar->pekerjaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Kegiatan Yang Diikuti</label>
                <input type="text" class="form-control" name="kegiatan" placeholder="Kegiatan Yang Diikuti" value="<?php echo (isset($pendaftar)) ? $pendaftar->kegiatan : "" ;?>">
            </div>
            <input type="hidden" class="form-control" name="id" value="<?php echo (isset($pendaftar)) ? $pendaftar->id : "" ;?>">
            <input type="submit" name="submit" value="Tambah Kegiatan Anggota Taruna" class="btn btn-primary btn-block">
        <?php echo form_close(); ?>
        </div>
        <div class="col-md-2">
        </div>
        
    </div>
</div>
