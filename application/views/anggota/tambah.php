<div class="container">
    <div class="row justify-content-md-center">
        <center><h1 class="text-center"><?=$title?></h1></center>
        <a href="<?php echo base_url() ?>anggota"><button class="btn btn-md btn-success">Lihat Anggota</button></a><br><br>
        <?php echo validation_errors(); ?>
        <?php echo (isset($posts)) ? form_open('anggota/update') : form_open('anggota/tambah') ?>
        <div class="col-md-6">       
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo (isset($posts)) ? $posts->nama_lengkap : "" ;?>">
            </div>
            <div class="form-group">
                <label>No KTP</label>
                <input type="text" class="form-control" name="no_ktp" placeholder="Nomor KTP" value="<?php echo (isset($posts)) ? $posts->no_ktp : "" ;?>">
            </div>
            <div class="form-group">
                <label>Tempat Lahir</label>
                <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir" value="<?php echo (isset($posts)) ? $posts->tempat_lahir : "" ;?>">
            </div>
            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input type="text" class="form-control" name="tanggal_lahir" placeholder="Tanggal Lahir" value="<?php echo (isset($posts)) ? $posts->tanggal_lahir : "" ;?>">
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="<?php echo (isset($posts)) ? $posts->alamat : "" ;?>">
            </div>
            <div class="form-group">
                <label>Nomor Handphone</label>
                <input type="text" class="form-control" name="nomor_telepon" placeholder="Nomor Telepon" value="<?php echo (isset($posts)) ? $posts->nomor_telepon : "" ;?>">
            </div>
        </div>
        <div class="col-md-6">       
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email Pengguna" value="<?php echo (isset($posts)) ? $posts->email : "" ;?>">
            </div>
            <div class="form-group">
                <label>Agama</label>
                <input type="text" class="form-control" name="agama" placeholder="Agama" value="<?php echo (isset($posts)) ? $posts->agama : "" ;?>">
            </div>
            <div class="form-group">
                <label>Pekerjaan</label>
                <input type="text" class="form-control" name="pekerjaan" placeholder="Pekerjaan" value="<?php echo (isset($posts)) ? $posts->pekerjaan : "" ;?>">
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                  <option value="laki-laki">Laki-laki</option>
                  <option value="perempuan">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label>Keterangan</label>
                <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" value="<?php echo (isset($posts)) ? $posts->keterangan : "" ;?>">
            </div>
        </div>
        <input type="hidden" class="form-control" name="id" value="<?php echo (isset($posts)) ? $posts->id : "" ;?>">
        <input type="submit" value="Tambah Anggota" name="submit" class="btn btn-primary btn-block">
        <?php echo form_close(); ?>
    </div>
</div>
