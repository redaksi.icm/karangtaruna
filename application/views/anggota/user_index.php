<div class="container">
<?php $this->load->view('templates/ms'); ?>
<center><h2><?=$title?></h2></center>
<br>
<table id="example" class="table table-striped table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama Lengkap</th>
                <th>No KTP</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Alamat</th>
                <th>Nomor Telepon</th>
                <th>Email</th>
                <th>Agama</th>
                <th>Pekerjaan</th>
                <th>Jenis Kelamin</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; foreach ($posts as  $post) :?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $post->nama_lengkap ?></td>
                <td><?= $post->no_ktp ?></td>
                <td><?= $post->tempat_lahir ?></td>
                <td><?= $post->tanggal_lahir ?></td>
                <td><?= $post->alamat ?></td>
                <td><?= $post->nomor_telepon ?></td>
                <td><?= $post->email ?></td>
                <td><?= $post->agama ?></td>
                <td><?= $post->pekerjaan ?></td>
                <td><?= $post->jenis_kelamin ?></td>
                <td><?= $post->keterangan ?></td>
            </tr>
            <?php $no++; endforeach; ?>
        </tbody>
</table>
<?php foreach ($tulisan as  $post) :?>
<h3><?php echo $post->title ?></h3>
<small class="post-date"><?php echo $post->created_at ?> in <strong><?php echo $post->name; ?></strong></small>
<div class="row ">
    <div class="col-md-3">
    <img  class="img-thumbnail img-fluid" src="<?php echo site_url().'assets/img/posts/'. $post->post_image ?>" alt="<?php echo $post->post_image ?>">
    </div>
    <div class="col-md-9">
        <p><?php echo word_limiter($post->body, 60) ?></p>
        <p><a class="btn btn-secondary" href="<?php echo site_url('/posts/'.$post->slug) ?>">Read More</a></p>
    </div>
</div>

<?php endforeach; ?>
<div class="pagination-links text-center">
<?php echo $this->pagination->create_links(); ?>
</div>
</div>
<br><br>
