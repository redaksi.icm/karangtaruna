<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Karang Taruna</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/datatables.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/ico.png" type="image/x-icon">
</head>
<body>
<div class="topnav" id="myTopnav" style="background: #eac435">
  <div style="align-content: left">
    <a style="pointer-events: none; cursor: default; font-size: 20px"><strong>KARANG TARUNA</strong></a>
    <?php if(!$this->session->userdata('logged_in')):?>
      <a href="<?php echo base_url() ?>posts">Kegiatan</a>
      <a href="<?php echo base_url() ?>lihat_anggota">Keanggotaan</a>
      <a href="<?php echo base_url() ?>lihat_jadwal">Informasi</a>
      <a href="<?php echo base_url() ?>kontak">Hubungi</a>
    <?php else:?>
      <a href="<?php echo base_url() ?>anggota">Data Anggota</a>
      <a href="<?php echo base_url() ?>jadwal">Jadwal Kegiatan</a>
      <a href="<?php echo base_url() ?>kelola">Pengelolaan Kegiatan</a>
      <!-- <a href="#about">Proposal Kegiatan</a> -->
    <?php endif; ?>
  </div>
  <div style="float: right;">
    <?php if(!$this->session->userdata('logged_in')):?>
      <a class="nav-link" href="<?php echo base_url() ?>users/login">Login</a>
    <?php else:?>
      <a class="nav-link" href="<?php echo base_url() ?>posts/create">Buat Artikel</a>
      <a class="nav-link" href="<?php echo base_url() ?>categories">Kategori</a>
      <a class="nav-link" href="<?php echo base_url() ?>users/register">Buat Akun</a>
      <a class="nav-link" href="<?php echo base_url() ?>users/logout">Logout</a>
    <?php endif; ?>
  </div>
</div>

