<div class="container">
    <div class="row justify-content-md-center">
        <center><h1>Informasi Kontak</h1></center>
        
        <div class="col-md-2">
        </div>
        <div class="col-md-8">    
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" class="form-control" value="Kabupaten Bogor" disabled="">
            </div>
            <div class="form-group">
                <label>Nomor Contact Person</label>
                <input type="text" class="form-control" value="0812992052xx" disabled="">
            </div>
            <div class="form-group">
                <label>Facebook</label>
                <input type="text" class="form-control" value="@karangtaruna" disabled="">
            </div>
            <div class="form-group">
                <label>Instagram</label>
                <input type="text" class="form-control" value="@karangtaruna" disabled="">
            </div>
            <center>
                <a href="<?php echo base_url() ?>formulir/formulir.pdf"><button class="btn btn-lg btn-success">Download Formulir Pendaftaran</button></a>
            </center>
        </div>
        <div class="col-md-2">
        </div>
        
    </div>
</div>
