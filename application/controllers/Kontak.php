<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Lihat Anggota';
        $this->load->view('templates/header');
        $this->load->view('kontak/index', $data);
        $this->load->view('templates/footer');
	}

}
