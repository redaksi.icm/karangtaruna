<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

    public function index(){
        $this->check_login();
        $data['posts'] = $this->anggota_model->get_anggota();
        $data['title'] = 'Lihat Anggota';
        $this->load->view('templates/header');
        $this->load->view('anggota/index', $data);
        $this->load->view('templates/footer');
    }

    public function user_index($offset = 0){
        $config['base_url'] = base_url().'posts/index';
        $config['total_rows'] = $this->db->count_all('posts');
        $config['per_page'] = 2;
        $config['uri_segment'] = 3;
        $config['attributes'] = array('class' => 'pagination-link');
        // init pagination
        $this->pagination->initialize($config);
        $data['title'] = 'Informasi Terbaru';
        $data['tulisan'] = $this->post_model->get_posts(false, $config['per_page'], $offset);

        $data['posts'] = $this->anggota_model->get_anggota();
        $data['title'] = 'Lihat Anggota';
        $this->load->view('templates/header');
        $this->load->view('anggota/user_index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah(){
        $this->check_login();
        $data['title'] = 'Formulir Data Anggota';
        if(!$this->input->post('submit')){
            $this->load->view('templates/header');
            $this->load->view('anggota/tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->anggota_model->create_anggota();
            $this->session->set_flashdata('success', 'Berhasil Menambahkan Anggota');
            redirect('anggota');
        }
    }
    
    public function delete($id){
        // check login
        $this->check_login();
        $this->anggota_model->delete_anggota($id);
        $this->session->set_flashdata('danger', 'Berhasil Menghapus Anggota');
        redirect('anggota');
    }

    public function edit($id){
        $this->check_login();
        $data['posts'] = $this->anggota_model->get_anggota_id($id);
        $data['title'] = 'Lihat Anggota';
        $this->load->view('templates/header');
        $this->load->view('anggota/tambah', $data);
        $this->load->view('templates/footer');
    }

    public function update(){
        $this->anggota_model->update_anggota();
        $this->session->set_flashdata('success', 'Berhasil Mengupdate Anggota');
        redirect('anggota');
    }


    private function check_login(){
        // check login
        if(!$this->session->userdata('logged_in')){
            redirect('users/login');
        }
    }
}
